'use strict';

const config = require('./config'),
    express = require('express'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    validator = require('express-validator'),
    socket = require('socket.io'),
    path = require('path'),
    http = require('http'),
    engines = require('consolidate'),
    routeAPIs = require('../routes/index'),
    constant = require('../const/siteconst');

module.exports.start = () => {
    let app = express();
    let server = http.createServer(app);
    let io = socket(server);

    app.set('views', path.join(appRoot, config.view_folders));
    app.set('view engine', 'html');
    app.set('public', path.join(appRoot, config.public_folders));
    app.use(express.static(path.join(__dirname, '../public')));

    app.use(bodyParser.json({ limit: "5mb" }));
    app.use(bodyParser.urlencoded({ extended: true, limit: "5mb" }));
    app.use(validator());
    app.use(cookieParser());

    // Add headers
    app.use((req, res, next) => {

        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', '*');

        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'access-control-allow-credentials, access-control-allow-headers, access-control-allow-methods, access-control-allow-origin, content-type, X-ACCESS_TOKEN, Authorization, x-requested-with');

        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', true);

        // Pass to next layer of middleware
        next();
    });

    app.use('/api', routeAPIs(io));
    app.use('/', (req, res) => {
        res.sendFile(path.join(__dirname, '../views/index.html'));
    });

    app.use((req, res, next) => {
        let err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    server.listen(config.port, () => {
        console.log(`Magical happen on port ${config.port}`);
    });
}