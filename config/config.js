'use strict';

const _ = require('lodash');
const path = require('path');

let initGlobalConfig = () => {
    if (!process.env.NODE_ENV) {
        process.env.NODE_ENV = 'dev';
    }

    let defaultConfig = require(path.join(appRoot, '/config/env/default.js'));

    let environmentConfig = require(path.join(appRoot, 'config/env', process.env.NODE_ENV)) || {};

    let config = _.merge(defaultConfig, environmentConfig);

    return config;
}

module.exports = initGlobalConfig();