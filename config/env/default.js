'use strict';

module.exports = {
    app: {
        title: 'Talkative',
        description: 'Realtime chat app',
        keywords: 'express, node.js, mysql'
    },
    port: 3000,
    view_folders: 'views',
    public_folders: 'public'
}