'use strict';

const constant = require('../const/siteconst');
let router = require('express').Router();

module.exports = io => {
    let chat = io.on('connection', socket => {
        console.log('A client connected');
    });

    return router;
}