'use strict';

const path = require('path');
global.appRoot = path.resolve(__dirname);

const app = require('./config/app');
app.start();