var constants = require('../const/siteconst');

module.exports = {
  getAPIResponse:  function(isSuccess, msg, data) {
    return {
      'status': isSuccess ? constants.API_SUCCESS_YES : constants.API_SUCCESS_NO,
      'msg': msg,
      'data': data
    };
  },
  socketEmmit: function (io, socketName, channel, data) {
    io.sockets.in(socketName).emit(channel, data);
  },
  getSocketName: function (hotelId, socketType, status) {
    return [hotelId, socketType, status].toString();
  },
  getChannel: function (socketType, status) {
    return [socketType, status].toString();
  }
};
